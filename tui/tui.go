package tui

import (
	"fmt"
	"math"

	"gitlab.com/go-sys-mon/go-sys-mon/util/info"

	"context"
	"time"

	"github.com/mum4k/termdash"
	"github.com/mum4k/termdash/cell"
	"github.com/mum4k/termdash/container"
	"github.com/mum4k/termdash/linestyle"
	"github.com/mum4k/termdash/terminal/tcell"
	"github.com/mum4k/termdash/terminal/terminalapi"
	"github.com/mum4k/termdash/widgets/donut"
	"github.com/mum4k/termdash/widgets/linechart"
	"github.com/mum4k/termdash/widgets/text"
)

var data *info.PCInfo

func getColor(idx int) cell.Color {
	colors := []cell.Color{
		cell.ColorMagenta,
		cell.ColorYellow,
		cell.ColorCyan,
		cell.ColorRed,
		cell.ColorFuchsia,
		cell.ColorLime,
		cell.ColorNavy,
		cell.ColorAqua,
		cell.ColorBlue,
		cell.ColorMaroon,
		cell.ColorOlive,
		cell.ColorTeal,
		cell.ColorPurple,
		cell.ColorRGB24(138, 43, 226),
		cell.ColorRGB24(255, 127, 80),
		cell.ColorRGB24(0, 255, 127),
	}

	if idx < len(colors) {
		return colors[idx]
	} else {
		return cell.ColorDefault
	}
}

func updateMemoryDonut(ctx context.Context, dnt *donut.Donut, delay time.Duration) {
	ticker := time.NewTicker(delay)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			var opts []donut.Option
			memUsed := int(math.Round(float64(data.Memory.Used) / float64(data.Memory.Total) * 100))

			if memUsed <= 50 {
				opts = append(opts, donut.CellOpts(cell.FgColor(getColor(5))))
			} else if memUsed <= 75 {
				opts = append(opts, donut.CellOpts(cell.FgColor(getColor(1))))
			} else {
				opts = append(opts, donut.CellOpts(cell.FgColor(getColor(3))))
			}

			text := fmt.Sprintf("%.1fGiB/%.1fGiB", (data.Memory.Used / 1024), (data.Memory.Total / 1024))
			opts = append(opts, donut.Label(text, cell.FgColor(getColor(8))))

			if err := dnt.Percent(memUsed, opts...); err != nil {
				panic(err)
			}
		case <-ctx.Done():
			return
		}
	}
}

func updateSwapDonut(ctx context.Context, dnt *donut.Donut, swapOn chan bool, delay time.Duration) {
	ticker := time.NewTicker(delay)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			if int(math.Round(data.Memory.Swap.Used/data.Memory.Swap.Total*100)) > 0 {
				swapOn <- true
			} else {
				swapOn <- false
			}

			var swapUsed int
			var opts []donut.Option
			swapUsed = int(math.Round(float64(data.Memory.Swap.Used) / float64(data.Memory.Swap.Total) * 100))
			if swapUsed <= 45 {
				opts = append(opts, donut.CellOpts(cell.FgColor(getColor(5))))
			} else if swapUsed <= 65 {
				opts = append(opts, donut.CellOpts(cell.FgColor(getColor(1))))
			} else {
				opts = append(opts, donut.CellOpts(cell.FgColor(getColor(3))))
			}

			text := fmt.Sprintf("%.1fGiB/%.1fGiB", (data.Memory.Swap.Used / 1024), (data.Memory.Swap.Total / 1024))
			opts = append(opts, donut.Label(text, cell.FgColor(getColor(4))))

			if err := dnt.Percent(swapUsed, opts...); err != nil {
				panic(err)
			}
		case <-ctx.Done():
			return
		}
	}
}

// Toggle swap donut once swapOn channel contains `true` value
func toggleSwapDonut(ctx context.Context, swapOn chan bool, cnt *container.Container, memDonut *donut.Donut, swapDonut *donut.Donut, delay time.Duration) {
	ticker := time.NewTicker(delay)
	defer ticker.Stop()
	val, lastVal := false, false

	for {
		select {
		case <-ticker.C:
			val = <-swapOn

			if val && (lastVal != val) {
				cnt.Update("Memory",
					container.Border(linestyle.None),
					container.SplitVertical(
						container.Left(
							container.BorderTitle("Memory"),
							container.Border(linestyle.Light),
							container.PlaceWidget(memDonut),
						),
						container.Right(
							container.BorderTitle("Swap"),
							container.Border(linestyle.Light),
							container.PlaceWidget(swapDonut),
						),
						container.SplitPercent(50),
					))
			} else if !val && (lastVal != !val) {
				cnt.Update("Memory", container.PlaceWidget(memDonut))
			}

			lastVal = val
		case <-ctx.Done():
			return
		}
	}
}

func updateCPUChart(ctx context.Context, lc *linechart.LineChart, delay time.Duration) {
	cpuData := make([][]float64, data.CPU.CoreCount)
	var cpuUsed []float64

	labels := map[int]string{}
	var sec int = 0

	ticker := time.NewTicker(delay)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			for j := 0; j <= sec; j++ {
				labels[j] = fmt.Sprintf("%ds", j)
			}

			cpuUsed = data.CPU.Load.Percent.PerCore
			for coreid := range cpuUsed {
				cpuData[coreid] = append(cpuData[coreid], cpuUsed[coreid])
			}

			for row := range cpuData {
				color := linechart.SeriesCellOpts(cell.FgColor(getColor(row)))
				if err := lc.Series(fmt.Sprintf("CPU%d", row), cpuData[row], color, linechart.SeriesXLabels(labels)); err != nil {
					panic(err)
				}
			}

			sec++
		case <-ctx.Done():
			return
		}
	}
}

func updateCPUText(ctx context.Context, textWidget *text.Text, delay time.Duration) {
	ticker := time.NewTicker(delay)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			out := fmt.Sprintf("AVG: %.0f%%\n", math.Round(data.CPU.Load.Percent.Average))
			textWidget.Write(out, text.WriteReplace())

			for i := range data.CPU.Load.Percent.PerCore {
				out = fmt.Sprintf("CPU%d: %.0f%%\n", i, math.Round(data.CPU.Load.Percent.PerCore[i]))
				textWidget.Write(out, text.WriteCellOpts(cell.FgColor(getColor(i))))
			}
		case <-ctx.Done():
			return
		}
	}
}

func updateDisksText(ctx context.Context, textWidget *text.Text, delay time.Duration) {
	ticker := time.NewTicker(delay)
	pad := []int{-10, -5, -5}

	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			out := fmt.Sprintf("%*s %*s %*s\n", pad[0], "Mount", pad[1], "Used", pad[2], "Size")
			textWidget.Write(out, text.WriteCellOpts(cell.Bold()), text.WriteReplace())

			for _, item := range data.Disks {
				perc_str := fmt.Sprintf("%.0f%%", item.UsedPercent)

				var space, mount string
				if item.TotalSize >= 1000000 {
					space = fmt.Sprintf("%.1f TB", item.TotalSize/1000000)
				} else if item.TotalSize >= 1000 {
					space = fmt.Sprintf("%.1f GB", item.TotalSize/1000)
				} else {
					space = fmt.Sprintf("%.1f MB", item.TotalSize)
				}

				if len(item.Mountpoint) > 20 {
					mount = fmt.Sprintf("%s…", item.Mountpoint[:20])
				} else {
					mount = item.Mountpoint
				}

				if float64(len(mount)) > math.Abs(float64(pad[0])) {
					pad[0] = -(len(mount) + 2)
				}

				out := fmt.Sprintf("%*s %*s %*s\n", pad[0], mount, pad[1], perc_str, pad[2], space)
				textWidget.Write(out)
			}
		case <-ctx.Done():
			return
		}
	}
}

func updateTempsText(ctx context.Context, textWidget *text.Text, delay time.Duration) {
	ticker := time.NewTicker(delay)
	pad := []int{-10, -5}

	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			out := fmt.Sprintf("%*s %*s\n", pad[0], "Sensor", pad[1], "°C")
			textWidget.Write(out, text.WriteCellOpts(cell.Bold()), text.WriteReplace())

			var opts []text.WriteOption

			for _, item := range data.Host.Temperatures {
				sensor := item.SensorName
				temp := item.Temperature
				tempHigh := float64(60)
				tempCritical := float64(75)

				if item.ThresholdHigh != 0 {
					tempHigh = item.ThresholdHigh
				}
				if item.ThresholdCritical != 0 {
					tempCritical = item.ThresholdCritical
				}

				if float64(len(sensor)) > math.Abs(float64(pad[0])) {
					pad[0] = -(len(sensor) + 2)
				}

				if temp >= tempCritical {
					opts = append(opts, text.WriteCellOpts(cell.FgColor(getColor(3))))
				} else if temp >= tempHigh {
					opts = append(opts, text.WriteCellOpts(cell.FgColor(getColor(1))))
				} else {
					opts = append(opts, text.WriteCellOpts(cell.FgColor(getColor(5))))
				}

				textWidget.Write(fmt.Sprintf("%*s ", pad[0], sensor))
				textWidget.Write(fmt.Sprintf("%*.1f\n", pad[1], temp), opts...)
			}
		case <-ctx.Done():
			return
		}
	}
}

func updateNetworkChart(ctx context.Context, lc *linechart.LineChart, delay time.Duration) {
	networkData := make([][]float64, 2)

	labels := map[int]string{}
	var sec int = 0

	ticker := time.NewTicker(delay)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			for j := 0; j <= sec; j++ {
				labels[j] = fmt.Sprintf("%ds", j)
			}

			networkData[0] = append(networkData[0], float64(data.Host.Network.BytesRecv/1048576))
			networkData[1] = append(networkData[1], float64(data.Host.Network.BytesSent/1048576))

			for row := range networkData {
				color := linechart.SeriesCellOpts(cell.FgColor(getColor(row)))
				if err := lc.Series(fmt.Sprintf("CPU%d", row), networkData[row], color, linechart.SeriesXLabels(labels)); err != nil {
					panic(err)
				}
			}

			sec++
		case <-ctx.Done():
			return
		}
	}
}

func updateNetworkText(ctx context.Context, recvTextWidget *text.Text, sentTextWidget *text.Text, delay time.Duration) {
	ticker := time.NewTicker(delay)
	converter := func(valBytes uint64) string {
		var result string

		if valBytes >= 1099511627776 {
			result = fmt.Sprintf("%.1fTiB", float64(valBytes/1099511627776))
		} else if valBytes >= 1073741824 {
			result = fmt.Sprintf("%.1fGiB", float64(valBytes/1073741824))
		} else if valBytes >= 1048576 {
			result = fmt.Sprintf("%.1fMiB", float64(valBytes/1048576))
		} else {
			result = fmt.Sprintf("%.1fKiB", float64(valBytes/1024))
		}

		return result
	}

	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			out := fmt.Sprintf("Sent: %s/s (Total: %s)",
				converter(data.Host.Network.BytesSent), converter(data.Host.Network.Total.BytesSent))
			sentTextWidget.Write(out, text.WriteReplace(), text.WriteCellOpts(cell.FgColor(getColor(1))))

			out = fmt.Sprintf("Received: %s/s (Total: %s)",
				converter(data.Host.Network.BytesRecv), converter(data.Host.Network.Total.BytesRecv))
			recvTextWidget.Write(out, text.WriteReplace(), text.WriteCellOpts(cell.FgColor(getColor(0))))

		case <-ctx.Done():
			return
		}
	}
}

func main() {
	t, err := tcell.New()
	if err != nil {
		panic(err)
	}
	defer t.Close()

	ctx, cancel := context.WithCancel(context.Background())

	memoryDonut, err := donut.New(
		donut.CellOpts(cell.FgColor(cell.ColorNumber(43))),
		donut.HolePercent(65),
		donut.Label("RAM Used"))
	if err != nil {
		panic(err)
	}
	swapDonut, err := donut.New(
		donut.CellOpts(cell.FgColor(cell.ColorNumber(172))),
		donut.HolePercent(65),
		donut.Label("Swap Used"))
	if err != nil {
		panic(err)
	}

	cpuChart, err := linechart.New(
		linechart.YAxisFormattedValues(linechart.ValueFormatterRoundWithSuffix("%")),
		linechart.YAxisCustomScale(0, 100),
		linechart.XAxisUnscaled(),
	)
	if err != nil {
		panic(err)
	}

	cpuText, err := text.New()
	if err != nil {
		panic(err)
	}

	disksText, err := text.New()
	if err != nil {
		panic(err)
	}
	tempsText, err := text.New()
	if err != nil {
		panic(err)
	}

	networkChart, err := linechart.New(
		linechart.YAxisFormattedValues(linechart.ValueFormatterRoundWithSuffix("MiB/s")),
		linechart.XAxisUnscaled(),
	)
	if err != nil {
		panic(err)
	}

	networkSentText, err := text.New(text.RollContent())
	if err != nil {
		panic(err)
	}
	networkRecvText, err := text.New(text.RollContent())
	if err != nil {
		panic(err)
	}

	c, err := container.New(
		t,
		container.FocusedColor(cell.ColorNumber(111)),
		container.SplitHorizontal(
			container.Top(
				container.Border(linestyle.Light),
				container.BorderTitle("CPU"),
				container.SplitVertical(
					container.Left(
						container.PlaceWidget(cpuChart),
					),
					container.Right(
						container.PlaceWidget(cpuText),
					),
					container.SplitPercent(90),
				)),
			container.Bottom(
				container.SplitVertical(
					container.Left(
						container.SplitHorizontal(
							container.Top(
								container.ID("Memory"),
								container.Border(linestyle.Light),
								container.BorderTitle("Memory"),
								container.PlaceWidget(memoryDonut),
							),
							container.Bottom(
								container.Border(linestyle.Light),
								container.BorderTitle("Network"),
								container.SplitHorizontal(
									container.Top(
										container.PlaceWidget(networkChart),
									),
									container.Bottom(
										container.SplitVertical(
											container.Left(
												container.PlaceWidget(networkSentText),
											),
											container.Right(
												container.PlaceWidget(networkRecvText),
											),
										),
									),
									container.SplitPercent(99),
								),
							),
							container.SplitPercent(45),
						),
					),
					container.Right(
						container.SplitHorizontal(
							container.Top(
								container.BorderTitle("Disks"),
								container.Border(linestyle.Light),
								container.PlaceWidget(disksText),
							),
							container.Bottom(
								container.BorderTitle("Temperatures"),
								container.Border(linestyle.Light),
								container.PlaceWidget(tempsText),
							),
							container.SplitPercent(60),
						),
					),
					container.SplitPercent(60),
				),
			),
			container.SplitPercent(40),
		),
	)
	if err != nil {
		panic(err)
	}

	delay := time.Second
	swapOn := make(chan bool)

	go updateCPUChart(ctx, cpuChart, delay)
	go updateCPUText(ctx, cpuText, delay)
	go updateDisksText(ctx, disksText, delay)
	go updateTempsText(ctx, tempsText, delay)
	go updateMemoryDonut(ctx, memoryDonut, delay)
	if data.Memory.Swap.Total != 0 {
		go updateSwapDonut(ctx, swapDonut, swapOn, delay)
		go toggleSwapDonut(ctx, swapOn, c, memoryDonut, swapDonut, delay)
	}
	go updateNetworkChart(ctx, networkChart, delay)
	go updateNetworkText(ctx, networkRecvText, networkSentText, delay)

	quitter := func(k *terminalapi.Keyboard) {
		if k.Key == 'q' || k.Key == 'Q' {
			cancel()
		}
	}

	if err := termdash.Run(ctx, t, c,
		termdash.KeyboardSubscriber(quitter), termdash.RedrawInterval(delay)); err != nil {
		panic(err)
	}
}

func InitTUI(inData *info.PCInfo) {
	data = inData
	main()
}
