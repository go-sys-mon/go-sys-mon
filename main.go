package main

import (
	"runtime"

	"gitlab.com/go-sys-mon/go-sys-mon/util"
)

func main() {
	runtime.Gosched()
	util.Start()
}
