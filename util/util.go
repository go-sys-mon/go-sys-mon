package util

import (
	"fmt"
	"os"

	"time"

	"github.com/pborman/getopt/v2"
	"gitlab.com/go-sys-mon/go-sys-mon/tui"
	"gitlab.com/go-sys-mon/go-sys-mon/util/cfg"
	"gitlab.com/go-sys-mon/go-sys-mon/util/info"
	"gitlab.com/go-sys-mon/go-sys-mon/webui"
)

var data info.PCInfo
var config cfg.ConfStruct

func update_data() {
	for {
		info.Fetch_info(&data)
		time.Sleep(time.Second)
	}
}

func parse_flags() (*string, *int) {
	getopt.HelpColumn = 40

	flagHelp := getopt.BoolLong("help", 'h', "This help message")
	uiMode := getopt.StringLong("ui-mode", 'u', "", "UI mode [tui, web]")
	listenPort := getopt.IntLong("port", 'p', 0, "Listen port")

	getopt.Parse()

	if *flagHelp {
		getopt.Usage()
		os.Exit(0)
	}

	if *listenPort == 0 {
		*listenPort = config.Port
	}

	return uiMode, listenPort
}

func Start() {
	config = cfg.GetConfig()
	uiMode, listenPort := parse_flags()

	go update_data()

	time.Sleep(500 * time.Millisecond)

	switch *uiMode {
	case "web":
		webui.Start(listenPort, &data)
	case "tui":
		tui.InitTUI(&data)
	default:
		fmt.Printf("ERROR: Invalid UI mode\n")
		getopt.Usage()
	}
}
