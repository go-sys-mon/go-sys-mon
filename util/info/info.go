package info

import (
	"log"
	"strings"

	psutil_cpu "github.com/shirou/gopsutil/v3/cpu"
	psutil_disk "github.com/shirou/gopsutil/v3/disk"
	psutil_host "github.com/shirou/gopsutil/v3/host"
	psutil_load "github.com/shirou/gopsutil/v3/load"
	psutil_mem "github.com/shirou/gopsutil/v3/mem"
	psutil_net "github.com/shirou/gopsutil/v3/net"
)

const megabyte = 1048576

type diskList struct {
	Device     string
	Mountpoint string
	FSType     string
	Opts       []string

	UsedPercent float64
	TotalSize   float64 // Megabytes
	Free        float64 // Megabytes
}

type temperaturesList struct {
	SensorName        string
	Temperature       float64
	ThresholdHigh     float64
	ThresholdCritical float64
}

type PCInfo struct {
	raw struct {
		CPU struct {
			Info    []psutil_cpu.InfoStat
			Load    *psutil_load.AvgStat
			Percent struct {
				PerCore []float64
				Average []float64
			}
			CoreCount int
		}
		Disk struct {
			PartitionList []psutil_disk.PartitionStat
		}
		Memory struct {
			MemInfo  *psutil_mem.VirtualMemoryStat
			SwapInfo *psutil_mem.SwapMemoryStat
		}
		NetworkIO []psutil_net.IOCountersStat
		Host      struct {
			Info       *psutil_host.InfoStat
			SensorStat []psutil_host.TemperatureStat
		}
	}

	CPU struct {
		Info []psutil_cpu.InfoStat
		Load struct {
			Percent struct {
				PerCore []float64
				Average float64
			}
		}
		CoreCount int
	}

	// RAM and Swap info in megabytes
	Memory struct {
		Swap struct {
			Total  float64
			Cached float64
			Free   float64
			Used   float64
		}
		Total     float64
		Free      float64
		Available float64
		Used      float64
	}

	Disks []diskList

	Host struct {
		Temperatures []temperaturesList
		Network      struct {
			Total struct {
				BytesSent uint64
				BytesRecv uint64
			}
			BytesSent uint64
			BytesRecv uint64
		}
	}
}

func handleErr(e error) {
	if e != nil && !strings.Contains(e.Error(), "warning") {
		log.Fatalln(e)
	}
}

func readRawInfo(raw_info *PCInfo) *PCInfo {
	var err error

	raw_info.raw.CPU.Info, err = psutil_cpu.Info()
	handleErr(err)

	raw_info.raw.CPU.Load, err = psutil_load.Avg()
	handleErr(err)

	raw_info.raw.CPU.Percent.PerCore, err = psutil_cpu.Percent(0, true)
	handleErr(err)

	raw_info.raw.CPU.Percent.Average, err = psutil_cpu.Percent(0, false)
	handleErr(err)

	raw_info.raw.CPU.CoreCount, err = psutil_cpu.Counts(true)
	handleErr(err)

	raw_info.raw.Disk.PartitionList, err = psutil_disk.Partitions(true)
	handleErr(err)

	raw_info.raw.Memory.MemInfo, err = psutil_mem.VirtualMemory()
	handleErr(err)

	raw_info.raw.Memory.SwapInfo, err = psutil_mem.SwapMemory()
	handleErr(err)

	raw_info.raw.NetworkIO, err = psutil_net.IOCounters(false)
	handleErr(err)

	raw_info.raw.Host.Info, err = psutil_host.Info()
	handleErr(err)

	raw_info.raw.Host.SensorStat, err = psutil_host.SensorsTemperatures()
	handleErr(err)

	return raw_info
}

func fetchCPUInfo(data *PCInfo) {
	data.CPU.Load.Percent.PerCore = data.raw.CPU.Percent.PerCore
	data.CPU.Load.Percent.Average = data.raw.CPU.Percent.Average[0]
	data.CPU.CoreCount = data.raw.CPU.CoreCount
	data.CPU.Info = data.raw.CPU.Info
}

func fetchMemoryInfo(data *PCInfo) {
	data.Memory.Total = float64(data.raw.Memory.MemInfo.Total) / megabyte
	data.Memory.Available = float64(data.raw.Memory.MemInfo.Available) / megabyte
	data.Memory.Free = float64(data.raw.Memory.MemInfo.Free) / megabyte
	data.Memory.Used = data.Memory.Total - data.Memory.Available
	data.Memory.Swap.Total = float64(data.raw.Memory.MemInfo.SwapTotal) / megabyte
	data.Memory.Swap.Cached = float64(data.raw.Memory.MemInfo.SwapCached) / megabyte
	data.Memory.Swap.Free = float64(data.raw.Memory.MemInfo.SwapFree) / megabyte
	data.Memory.Swap.Used = float64(data.raw.Memory.MemInfo.SwapTotal-data.raw.Memory.MemInfo.SwapFree) / megabyte
}

func fetchDiskInfo(data *PCInfo) {
	contains := func(arr []string, str string) bool {
		for _, a := range arr {
			if a == str {
				return true
			}
		}
		return false
	}

	fs_list := []string{"ext4", "ext3", "btrfs", "f2fs", "reiserfs", "udf", "ntfs", "vfat", "jfs", "xfs", "zfs"}
	data.Disks = nil
	for _, item := range data.raw.Disk.PartitionList {
		if contains(fs_list, item.Fstype) {
			var disk diskList

			disk.Device = item.Device
			disk.FSType = item.Fstype
			disk.Mountpoint = item.Mountpoint
			disk.Opts = item.Opts

			data.Disks = append(data.Disks, disk)
		}
	}

	for i, item := range data.Disks {
		usg, err := psutil_disk.Usage(item.Mountpoint)
		handleErr(err)

		item.UsedPercent = usg.UsedPercent
		item.TotalSize = float64(usg.Total / megabyte)
		item.Free = float64(usg.Free / megabyte)

		data.Disks[i] = item
	}
}

func fetchTempsInfo(data *PCInfo) {
	data.Host.Temperatures = nil
	for _, rawSensor := range data.raw.Host.SensorStat {
		var sensor temperaturesList

		sensor.SensorName = rawSensor.SensorKey
		sensor.Temperature = rawSensor.Temperature
		sensor.ThresholdHigh = rawSensor.High
		sensor.ThresholdCritical = rawSensor.Critical

		if sensor.Temperature != 0 {
			data.Host.Temperatures = append(data.Host.Temperatures, sensor)
		}
	}
}

func fetchNetworkInfo(data *PCInfo) {
	lastTotalBytesSent := data.Host.Network.Total.BytesSent
	lastTotalBytesRecv := data.Host.Network.Total.BytesRecv

	data.Host.Network.Total.BytesSent = data.raw.NetworkIO[0].BytesSent
	data.Host.Network.Total.BytesRecv = data.raw.NetworkIO[0].BytesRecv

	data.Host.Network.BytesSent = data.Host.Network.Total.BytesSent - lastTotalBytesSent
	data.Host.Network.BytesRecv = data.Host.Network.Total.BytesRecv - lastTotalBytesRecv
}

func Fetch_info(data *PCInfo) error {
	readRawInfo(data)

	go fetchCPUInfo(data)
	go fetchMemoryInfo(data)
	go fetchDiskInfo(data)
	go fetchTempsInfo(data)
	go fetchNetworkInfo(data)

	return nil
}
