require.config({
    paths: { "bcrypt": "/static/lib/bcrypt.min" },
});

var loginData;
fetchData();

const loginForm = document.getElementById("login-form");
const loginButton = document.getElementById("login-form-submit");
const loginErrorMsg = document.getElementById("login-error-msg");

loginButton.addEventListener("click", (e) => {
    e.preventDefault();
    const username = loginForm.username.value;
    const password = loginForm.password.value;

    require(["bcrypt"], function(bcrypt) {
        if (username === loginData.Username && bcrypt.compareSync(password, loginData.Password)) {
            sessionStorage.setItem("isLoggedIn", "true");
            window.location.href = "/";
        } else {
            loginErrorMsg.style.opacity = 1;
        }
    });
})

function fetchData() {
    $.getJSON({
        url: '/api/login',
        success: function(response) {
            loginData = response;
        }
    })
}