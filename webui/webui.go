package webui

import (
	"embed"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"

	"gitlab.com/go-sys-mon/go-sys-mon/util/cfg"
	"gitlab.com/go-sys-mon/go-sys-mon/util/info"
)

var (
	//go:embed static
	//go:embed template
	content embed.FS
	data    *info.PCInfo
	pages   = map[string]string{
		"/":      "template/index.gohtml",
		"/login": "template/login.gohtml",
	}
)

func Start(listenPort *int, info *info.PCInfo) {
	data = info
	initWebServer(listenPort)
}

func initWebServer(listenPort *int) {
	port := ":" + fmt.Sprint(*listenPort)
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/", indexPage)
	http.HandleFunc("/login", loginPage)
	http.HandleFunc("/api", getData)
	http.HandleFunc("/api/login", getLoginInfo)
	http.Handle("/static/", http.FileServer(http.FS(content)))
	http.FileServer(http.FS(content))
	template.ParseFS(content, "*")

	fmt.Printf("Starting web server at http://%s%s\n", hostname, port)
	if err := http.ListenAndServe(port, nil); err != nil {
		log.Fatalf("Error launching WebUI: [%s]", err)
	}
}

func indexPage(w http.ResponseWriter, r *http.Request) {
	page, ok := pages[r.URL.Path]
	if !ok {
		http.NotFound(w, r)
		return
	}

	tpl, err := template.ParseFS(content, page)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	if err := tpl.Execute(w, data); err != nil {
		return
	}
}

func loginPage(w http.ResponseWriter, r *http.Request) {
	page, ok := pages[r.URL.Path]
	if !ok {
		http.NotFound(w, r)
		return
	}

	tpl, err := template.ParseFS(content, page)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	if err := tpl.Execute(w, data); err != nil {
		return
	}
}

func getData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func getLoginInfo(w http.ResponseWriter, r *http.Request) {
	loginInfo := cfg.GetConfig()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(loginInfo)
}
