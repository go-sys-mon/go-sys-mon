# go-sys-mon

## go-sys-mon - A system monitoring tool for Linux written in Go
[![pipeline status](https://gitlab.com/go-sys-mon/go-sys-mon/badges/main/pipeline.svg)](https://gitlab.com/go-sys-mon/go-sys-mon/-/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/go-sys-mon/go-sys-mon)](https://goreportcard.com/report/gitlab.com/go-sys-mon/go-sys-mon)

*go-sys-mon* supports monitoring of CPU, RAM, disks, network and temperatures. The program features two UI modes: Web UI and Terminal UI.

![webui](screenshots/webui.png)
![tui](screenshots/tui.png)

### Config
Web UI can be configured using `config.yaml`, which has to be located at `$HOME/.config/go-sys-mon/config.yaml` or OS's equivalent. 
Sample `config.yaml` can be found in `util/cfg`. It's automatically loaded when user's config does not exist.

```yaml
port: 8000
username: admin
password: password
```

Default values are **insecure**, so it is strongly advised to change them.

### Options
```
 -h, --help           Help message
 -p, --port=value     Listen port [8000]
 -u, --ui-mode=value  UI mode [web] (web, tui)
```

### Used libraries
Backend (Go):
1. [gopsutil](https://github.com/shirou/gopsutil)
2. [getopt](https://github.com/pborman/getopt)

Terminal UI (Go):
1. [termdash](https://github.com/mum4k/termdash)

Web UI (JavaScript, CSS):
1. [ApexCharts](https://apexcharts.com/)
2. [jQuery](https://jquery.com/)
3. [bcrypt.js](https://github.com/dcodeIO/bcrypt.js/)
4. [Bootstrap 5](https://getbootstrap.com/)

### License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
